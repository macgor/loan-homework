package pl.gorzala.web.rest;

import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST for check health of application
 */
@RestController
@RequestMapping("/api/ping")
@Api(description = "Health-check operation.")
public class PingResource {

    @GetMapping("/")
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok().body("OK");
    }
}
