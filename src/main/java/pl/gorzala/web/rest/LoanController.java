package pl.gorzala.web.rest;


import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.gorzala.loan.dto.LoanRequest;
import pl.gorzala.loan.service.LoanService;

/**
 * REST for loan application
 */
@RequestMapping("/api/loan")
@RestController
@Api(description = "Loan operation.")
public class LoanController {

    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    private final LoanService loanService;


    @PostMapping("/")
    public ResponseEntity<?> applyForLoad(@RequestBody LoanRequest request) {
        if (request == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        loanService.applyLoan(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @GetMapping("/{pesel}/")
    public ResponseEntity<?> findByPesel(@PathVariable("pesel") String pesel) {
        if (pesel == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(loanService.findByPesel(pesel), HttpStatus.OK);
    }

    @PutMapping("/{pesel}/{date}/")
    public ResponseEntity<?> findByPesel(@PathVariable("pesel") String pesel, @PathVariable("date") String date) {
        loanService.extendLoan(pesel, date);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
