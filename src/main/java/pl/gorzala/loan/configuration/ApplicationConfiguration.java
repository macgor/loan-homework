package pl.gorzala.loan.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Configuration for application properties
 */
@Getter
@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfiguration {

    @Value("${loan.amount.max}")
    private Integer maxLoanAmount;
    @Value("${loan.amount.min}")
    private Integer minLoanAmount;
    @Value("${loan.time.max}")
    private Integer maxLoanTime;
    @Value("${loan.time.min}")
    private Integer minLoanTime;
    @Value("${loan.term.max}")
    private Integer loanMaxTerm;
    @Value("${loan.term.min}")
    private Integer loanMinTerm;
    @Value("${loan.lending.rate}")
    private Integer loanLendingRate;


    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
