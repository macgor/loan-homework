package pl.gorzala.loan.service;

import lombok.AllArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;
import pl.gorzala.loan.configuration.ApplicationConfiguration;
import pl.gorzala.loan.dto.LoanRequest;
import pl.gorzala.loan.dto.LoanResponse;
import pl.gorzala.loan.model.Loan;
import pl.gorzala.loan.model.Status;
import pl.gorzala.loan.repository.LoanRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Service of loan operations
 */
@AllArgsConstructor
@Service
public class LoanService {

    private final LoanRepository loanRepository;

    private final ApplicationConfiguration applicationConfiguration;

    /**
     * Find loan by pesel
     * @param pesel personal number
     * @return loan response object
     */
    public LoanResponse findByPesel(String pesel) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        return loanRepository.findByPesel(pesel).map(m -> LoanResponse.builder()
                .status(Status.ACCEPTED)
                .amount(m.getAmount())
                .fullName(m.getFullName())
                .payOffDate(formatter.format(m.getPayOffDate()))
                .build()).orElse(LoanResponse.builder()
                .status(Status.REJECTED)
                .build());
    }

    /**
     * Extend {@link Loan} pay off date
     * @param pesel personal number
     * @param term extend date
     */
    public void extendLoan(String pesel, String term) {
        if (StringUtils.isBlank(pesel) || StringUtils.isBlank(term)) {
            throw new IllegalArgumentException("Applicated values cannot be null!");
        }

        Loan loan = loanRepository.findByPesel(pesel).orElseThrow(IllegalArgumentException::new);
        int days = Days.daysBetween(new LocalDate(loan.getPayOffDate()), new LocalDate(term)).getDays();

        DateTime dateTime = new DateTime(loan.getPayOffDate());
        dateTime = dateTime.plusDays(days);
        loan.setPayOffDate(dateTime.toDate());

        loanRepository.save(loan);
    }

    /**
     * Application for new {@link Loan}
     * @param loanRequest loan data object
     */
    public void applyLoan(LoanRequest loanRequest) {
        if (loanRequest == null) {
            return;
        }

        calculate(loanRequest);

        BigDecimal amount = new BigDecimal(loanRequest.getAmount());

        Loan loan = new Loan();
        loan.setCreateDate(new Date());
        loan.setPayOffDate(loanRequest.getPayOffDate());
        loan.setZipCode(loanRequest.getZipCode());
        loan.setAddress(loanRequest.getAddress());
        loan.setCity(loanRequest.getCity());
        loan.setCountry(loanRequest.getCountry());
        loan.setFullName(loanRequest.getFullName());
        loan.setPesel(loanRequest.getPesel());

        BigDecimal loanTotalAmount = amount.multiply(new BigDecimal(100 + applicationConfiguration.getLoanLendingRate())).scaleByPowerOfTen(-2)
                .setScale(2, RoundingMode.CEILING);

        loan.setAmount(loanTotalAmount);

        loanRepository.save(loan);
    }

    private void calculate(LoanRequest loanRequest) {
        if (!NumberUtils.isDigits(loanRequest.getAmount()) || !amountBetween(loanRequest.getAmount())) {
            throw new IllegalArgumentException("Application amount exceeded the range!");
        }
        if (!termBetween(new Date(), loanRequest.getPayOffDate())) {
            throw new IllegalStateException("Application term value is incompatible!");
        }
        BigDecimal amount = new BigDecimal(loanRequest.getAmount());
        if (timeBetween() && amount.compareTo(BigDecimal.valueOf(applicationConfiguration.getMaxLoanAmount())) == 0) {
            throw new IllegalStateException("Application amount max reach value and incompatible time range!");
        }
    }

    private boolean timeBetween() {
        int from = applicationConfiguration.getMinLoanTime();
        int to = applicationConfiguration.getMaxLoanTime();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int t = c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE);

        return to > from && t >= from && t <= to || to < from && (t >= from || t <= to);
    }

    private boolean termBetween(Date startDate, Date endDate) {
        int days = Days.daysBetween(new LocalDate(startDate), new LocalDate(endDate)).getDays();

        return applicationConfiguration.getLoanMaxTerm() > days && applicationConfiguration.getLoanMinTerm() < days;
    }

    private boolean amountBetween(String value) {
        BigDecimal amount = new BigDecimal(value);

        return amount.compareTo(BigDecimal.valueOf(applicationConfiguration.getMinLoanAmount())) > 0
                && amount.compareTo(BigDecimal.valueOf(applicationConfiguration.getMaxLoanAmount())) < 0;

    }

}
