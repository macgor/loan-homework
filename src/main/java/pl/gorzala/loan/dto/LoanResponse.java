package pl.gorzala.loan.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.gorzala.loan.model.Status;

import java.math.BigDecimal;

/**
 * Loan response data object
 */
@Builder
@Getter
@Setter
public class LoanResponse {

    private Status status;
    private String message;
    private BigDecimal amount;
    private String fullName;
    private String payOffDate;

}
