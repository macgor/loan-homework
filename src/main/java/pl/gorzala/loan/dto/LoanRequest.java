package pl.gorzala.loan.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Loan request data object
 */
@Getter
@Setter
public class LoanRequest {

    private String fullName;
    private String pesel;
    private String address;
    private String zipCode;
    private String city;
    private String country;
    private String amount;
    private Date payOffDate;

}
