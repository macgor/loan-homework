package pl.gorzala.loan.model;

/**
 * Statuses for {@link Loan}
 */
public enum Status {

    ACCEPTED,
    REJECTED

}
