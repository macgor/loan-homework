package pl.gorzala.loan.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "loan")
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;
    @Column(name = "FULL_NAME")
    private String fullName;
    @Column(name = "PESEL")
    private String pesel;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "ZIP_CODE")
    private String zipCode;
    @Column(name = "CITY")
    private String city;
    @Column(name = "COUNTRY")
    private String country;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "CREATE_DATE")
    @Type(type = "timestamp")
    private Date createDate;
    @Column(name = "PAY_OFF_DATE")
    @Type(type = "timestamp")
    private Date payOffDate;
}
