package pl.gorzala.loan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.gorzala.loan.model.Loan;

import java.util.Optional;

/**
 * Repository interface for loan
 */
public interface LoanRepository extends JpaRepository<Loan, Integer> {

    Optional<Loan> findByPesel(String pesel);


}
