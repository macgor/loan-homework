package pl.gorzala;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.gorzala.loan.configuration.ApplicationConfiguration;
import pl.gorzala.loan.dto.LoanRequest;
import pl.gorzala.loan.dto.LoanResponse;
import pl.gorzala.loan.model.Loan;
import pl.gorzala.loan.model.Status;
import pl.gorzala.loan.repository.LoanRepository;
import pl.gorzala.loan.service.LoanService;

import java.util.Date;
import java.util.Optional;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public class LoanServiceTest {

    private LoanService loanService;

    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    private LoanRepository loanRepositoryMock;

    @Before
    public void init() {
        loanRepositoryMock = mock(LoanRepository.class);
        loanService = new LoanService(loanRepositoryMock, applicationConfiguration);
    }

    @Test
    public void findByPesel_correctPesel_expectSuccess() {
        // given
        String pesel = "91120112201";

        // when
        when(loanRepositoryMock.findByPesel(pesel)).thenReturn(Optional.of(prepareLoan()));
        LoanResponse loan = loanService.findByPesel(pesel);

        // then
        assertNotNull(loan);
        assertSame(loan.getStatus(), Status.ACCEPTED);
    }

    @Test
    public void findByPesel_incorrectPesel_expectSuccess() {
        // given
        String pesel = "91120112201";
        String peselMismatch = "10221102119";

        // when
        when(loanRepositoryMock.findByPesel(peselMismatch)).thenReturn(Optional.of(prepareLoan()));
        LoanResponse loan = loanService.findByPesel(pesel);

        // then
        assertNotNull(loan);
        assertSame(loan.getStatus(), Status.REJECTED);
    }

    @Test
    public void extendLoan_correctData_expectSuccess() {
        // given
        String pseel = "91120112201";

        // when
        when(loanRepositoryMock.findByPesel(pseel)).thenReturn(Optional.of(prepareLoan()));
        loanService.extendLoan(pseel, "2019-01-21");
    }

    @Test
    public void extendLoan_incorrectData_expectSuccess() {
        // given
        String pesel = "91120112201";
        String peselMismatch = "10221102119";

        // when
        when(loanRepositoryMock.findByPesel(pesel)).thenReturn(Optional.of(prepareLoan()));

        try {
            loanService.extendLoan(peselMismatch, "2019-01-21");
            fail("Except exception");
        } catch (IllegalArgumentException e) {
            // then
            assertNotNull(e);
        }
    }

    @Test
    public void applyLoan_correctData_expectSuccess() {
        // given
        LoanRequest loanRequest = new LoanRequest();
        loanRequest.setAmount("1000");
        loanRequest.setPesel("91120112201");
        loanRequest.setPayOffDate(textToDate("2019-01-25"));

        // when
        loanService.applyLoan(loanRequest);
    }

    @Test
    public void applyLoan_incorrectData_expectSuccess() {
        // given
        LoanRequest loanRequest = new LoanRequest();
        loanRequest.setAmount("10050");
        loanRequest.setPesel("91120112201");
        loanRequest.setPayOffDate(textToDate("2019-01-25"));

        try {
            // when
            loanService.applyLoan(loanRequest);
            fail("Except exception");
        } catch (IllegalArgumentException e) {
            // then
            assertNotNull(e);
        }
    }


    private Loan prepareLoan() {
        Loan loan = new Loan();
        loan.setPayOffDate(new Date());


        return loan;
    }

    private Date textToDate(String textDate) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("YYYY-MM-DD");
        DateTime dt = formatter.parseDateTime(textDate);
        return dt.toDate();
    }

}
